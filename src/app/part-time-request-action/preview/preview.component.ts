import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService} from '@universis/common';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html'
})
export class PreviewComponent implements OnInit, OnDestroy {

  public data: any;
  public department: any;
  public loading = true;
  public partTimeRequestAction: any;
  private paramSubscription?: Subscription;

  constructor(private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _activatedRoute: ActivatedRoute) {
  }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async ngOnInit() {
    this.showLoading(true);
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      this.partTimeRequestAction = await this._context.model('StudentRequestActions')
          .asQueryable()
          .where('code').equal(params['code'])
          .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
          .getItem();
      this.showLoading(false);
    });
  }
}
