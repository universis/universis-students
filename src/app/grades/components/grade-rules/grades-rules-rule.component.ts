import { Component, Input, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';
import { LoadingService } from '@universis/common';

@Component({
  selector: 'app-grades-rules-rule',
  templateUrl: './grades-rules-rule.component.html',
  styleUrls: ['./grades-rules-rule.component.scss']
})
export class GradesRulesRuleComponent implements OnInit {

  @Input("rule") rule;
  /**
   * Courses, Units, Ects, Hours
   */
  @Input("projection") projection;

  public fieldTotal: string = "";
  public fieldPassed: string = "";
  public subfieldTotal: string = "";
  public subfieldPassed: string = "";

  public relevantSubrules: any[] = [];
  public valid: boolean = false;

  constructor() {
  }

  ngOnInit() {
    // projection fields
    this.fieldTotal = "total" + this.projection;
    this.fieldPassed = "totalPassed" + this.projection;
    this.subfieldTotal = this.projection.toLowerCase();
    this.subfieldPassed = "totalPassed" + this.projection;
    if (this.projection == "Courses") {
      this.fieldPassed = "totalPassed";
      this.subfieldPassed = "totalPassed";
    }
    this.rule.rules.forEach(subrule => {
      if (subrule.courseTypes) {
        subrule.courseTypesNames = subrule.courseTypes.map(i => i.locale?.name || i.name).join(", ");
      }
    })
    // prepare
    this.valid = this.rule[this.fieldPassed] >= this.rule[this.fieldTotal];
    this.relevantSubrules = this.rule.rules.filter(i => i.totals[this.subfieldTotal] > 0);
    if (this.relevantSubrules.length > 0 && this.valid) {
      this.valid = this.relevantSubrules.every(i => i.totals[this.subfieldPassed] >= i.totals[this.subfieldTotal])
    }
    // when rules have a courseType
    if (this.relevantSubrules.length > 0) {
      // handling of special cases for the rules in order to display
      // usable and readable information in all cases

      // create the totals based on the sum of the rules
      const totals = this.relevantSubrules.reduce((acc, i) => acc + i.totals[this.subfieldTotal], 0);
      const passed = this.relevantSubrules.reduce((acc, i) => acc + Math.min(i.totals[this.fieldPassed], i.totals[this.subfieldTotal]), 0)
      if (this.rule[this.fieldTotal] == 0) {
        // convert Courses: 0/0 SomeType: 2/3
        // to      Courses: 2/3 SomeType: 2/3
        this.rule[this.fieldTotal] = totals;
        this.rule[this.fieldPassed] = passed;
      } else if (totals > this.rule[this.fieldTotal]) {
        // convert Courses: 6/6 SomeType: 6/13
        // to      Courses: 6/13 SomeType: 6/13
        this.rule[this.fieldTotal] = totals;
        this.rule[this.fieldPassed] = passed;
      } else {
        // convert Courses: 7/3 SomeType: 0/1
        // to      Courses: 2/3 SomeType: 0/1
        const genericTotal = this.rule[this.fieldTotal] - totals;
        const genericPassed = Math.min(this.rule[this.fieldPassed], genericTotal);
        this.rule[this.fieldPassed] = genericPassed + passed;
      }
    }
  }

}