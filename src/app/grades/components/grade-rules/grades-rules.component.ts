import { Component, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';
import { LoadingService } from '@universis/common';

@Component({
  selector: 'app-grades-theses',
  templateUrl: './grades-rules.component.html',
  styleUrls: ['./grades-rules.component.scss']
})
export class GradesRulesComponent implements OnInit {

  public rules: any;
  /** If some rules are skipped */
  public skipped: boolean = false;
  /** If some semesters do not have corresponding semester rules */
  public trimmed: boolean = false;
  public updated: boolean = true;

  public isLoading = true;

  constructor(private gradesService: GradesService,
    private loadingService: LoadingService) {

    this.loadRules();
  }

  loadRules() {
    // this.isLoading = true;
    this.loadingService.showLoading();
    this.gradesService.getProgramSemesterRules(this.updated).then((rules) => {
      this.updateRules(rules);
      this.loadingService.hideLoading();
      this.isLoading = false;
    }).catch(err => {
      this.loadingService.hideLoading();
      this.isLoading = false;
      console.log(err);
    });
  }

  ngOnInit() {
  }

  updateRules(rules: any[]) {

    // skip rules that have special values on the semester rules
    // - null is defined as 'remaining semesters'
    // - >250 are defined as related to win spr instead of the ordinal  
    const simpleRules = rules.filter(i => {
      return i.semester != null && i.semester <= 250
    })
    this.skipped = rules.length != simpleRules.length
    rules = simpleRules;

    // trim the rules, remove all empty rules for the last semester going backwards
    // if a semester in the middle has rules will be shown as empty, the trailing 
    // empty semesters will be removed
    this.trimmed = false;
    while (rules.length > 0) {
      const last = rules[rules.length - 1];
      if (last.totalCourses + last.totalUnits + last.totalEcts + last.totalHours + last.rules.length == 0) {
        // is empty
        rules.pop();
        this.trimmed = true;
      } else {
        break;
      }
    }

    this.rules = rules;
  }

  setUpdated(value) {
    this.updated = value;
    this.loadRules();
  }

}
