import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../services/grades.service';
import {Router} from '@angular/router';
import { ApplicationSettingsConfiguration, ConfigurationService } from '@universis/common';

export declare interface LocalApplicationSettings extends ApplicationSettingsConfiguration {
  showSemesterRules: boolean;
}

@Component({
  selector: 'app-grades-home',
  templateUrl: './grades-home.component.html',
  styleUrls: ['./grades-home.component.scss']
})
export class GradesHomeComponent implements OnInit {
  viewMode = 'tab1';

  public showSemesterRules;

  constructor(
    private gradeService: GradesService,
    private _configurationService: ConfigurationService,
    private _router: Router) {

    this.showSemesterRules = (this._configurationService?.settings?.app as LocalApplicationSettings).showSemesterRules;
  }

  ngOnInit() {
    if (this._router.url.toString() === '/grades/recent') {
      this.viewMode = 'tab1';
    } else  if (this._router.url.toString() === '/grades/all') {
      this.viewMode = 'tab2';
    } else  if (this._router.url.toString() === '/grades/theses') {
      this.viewMode = 'tab3';
    } else  if (this._router.url.toString() === '/grades/internship') {
      this.viewMode = 'tab4';
    } else  if (this._router.url.toString() === '/grades/rules') {
      this.viewMode = 'tab5';
    }
  }

}
