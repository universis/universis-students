import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {asyncMemoize, GradeScale, GradeScaleService} from '@universis/common';
import {round} from 'mathjs';
import { ProfileService } from 'src/app/profile/services/profile.service';

export declare interface CourseGradeBase {
  // defines the structure of the course (simple, complex, coursePart etc)
  // can either be an object or a number
  courseStructureType: any;
  // the number of units the course provides
  units: number;
  // defines if the grade is calculated in the degree grade
  calculateGrade: boolean;
  // defines if the units the course provides are counted in the total units
  calculateUnits: boolean;
  // defines if the course is passed
  isPassed: boolean;
  // the number of ects the course provides
  ects: number;
  // the coefficient of the course toward degree-getting
  coefficient?: number | any;
  // the grade of the course
  grade: number;
}

export declare interface GradeAverageResult {
  // the total number of courses
  courses: number;
  // the total number of passed courses
  passed: number;
  // the sum of grades used for calculating average
  grades: number;
  // the sum of coefficients used for calculating a weighted average grade
  coefficients?: number | any;
  // the calculated average grade
  average: number;
  // the total number of ects of passed courses
  ects: number;
  // the total number of ects of all the courses 
  ectsTotal?: number;
  // the total number of units of passed courses
  units: number;
}

export declare interface CourseGradeDistributionBase {
  name: string,
  displayCode: string,
  examPeriodName: string,
  examPeriodYear: string,
  gradeScaleId: number,
  examId: number
}

@Injectable()
export class GradesService {

  constructor(private _context: AngularDataContext, private _gradeScaleService: GradeScaleService,  private _profileService: ProfileService) { }
  @asyncMemoize()
    getAllGrades(): any {
        return this._context.model('students/me/grades')
            .asQueryable()
            .expand('course, courseExam($expand=examPeriod,year)')
            .orderByDescending('courseExam/year')
            .thenByDescending('courseExam/examPeriod')
            .thenByDescending('grade1')
            .take(-1)
            .getItems();
    }

    getCourseTeachers(): any {
        return this._context.model('students/me/classes')
            .asQueryable()
            .select('course/id as id, courseClass')
            .expand('courseClass($expand=instructors($expand=instructor($select=InstructorSummary)))')
            .take(-1)
            .getItems();
    }
    @asyncMemoize()
    getGradeInfo(): any {
        return this._context.model('students/me/courses')
            .asQueryable()
            .expand('course($expand=locale)', 'courseType($expand=locale)', 'gradeExam($expand=instructors($expand=instructor($select=InstructorSummary)))')
            .orderByDescending('semester')
            .thenByDescending('gradeYear')
            .take(-1)
            .getItems();
    }
    getDefaultGradeScale(): Promise<GradeScale> {
      // get study program through profile
      return this._profileService.getStudent().then(student => {
        return this._gradeScaleService.getGradeScale(
          student.studyProgram.gradeScale
        );
      });
    }

  getThesisInfo(): any {
    return this._context.model('students/me/theses')
      .asQueryable()
      .expand('results($orderby=index;$expand=instructor($select=InstructorSummary)),thesis($expand=instructor($select=InstructorSummary),locale)')
      .take(-1)
      .getItems();
  }

  getInternshipInfo(): any {
    return this._context.model('students/me/internships')
      .asQueryable()
      .expand('status, company, locale')
      .take(-1)
      .getItems();
  }

  getProgramSemesterRules(updated: boolean): any {
    if (!updated) {
      return this._context.model('students/me/ProgramSemesterRules')
        .asQueryable()
        .take(-1)
        .getItems();
    } else {
      // return this._context.model('students/me/ProgramSemesterRulesUpdated')
      //   .asQueryable()
      //   .take(-1)
      //   .getItems();
      return this.calculateRulesPerCourseSemester();
    }
  }

  @asyncMemoize()
  async calculateRulesPerCourseSemester() {
    const context = this._context;
    const student = await this._profileService.getStudent();

    // get study program semester rules
    const semesterRules = await context.model('StudyProgramSemesterRules')
      .where('studyProgram').equal(student.studyProgram.id)
      .and('additionalType').equal('SemesterRule')
      .expand('courseTypes($expand=locale)')
      .orderBy('semester')
      .take(-1)
      .getItems();

    // get semesters from the rules
    const studentSemesters = semesterRules.map(i => i.semester)
      .filter((i, index, array) => array.indexOf(i) === index) // uniq
      .sort((a, b) => (a || 1000) - (b || 1000)) // nulls last
      .map(semester => ({ semester }))

    // get total passed courses grouped by course type, year, period
    const studentCourses = await context.model('students/me/courses')
      .where('isPassed').equal(1)
      .and('courseStructureType').lowerOrEqual(4)
      .select(
        'count(id) as totalCourses',
        'sum(units) as totalUnits',
        'sum(ects) as totalEcts',
        'sum(hours) as totalHours',
        'semester/id as semester',
        'courseType/id as courseType'
      )
      .groupBy('semester/id', 'courseType/id')
      .getItems();

    // calculate totals
    studentSemesters.forEach(studentSemester => {
      // get total passed for specific period
      const studentTotals = studentCourses.filter(x => {
        return studentSemester.semester === x.semester
      });
      studentSemester.totalPassed = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalCourses, 0);
      studentSemester.totalPassedEcts = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalEcts, 0);
      studentSemester.totalPassedUnits = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalUnits, 0);
      studentSemester.totalPassedHours = studentTotals.reduce((partial_sum, a) => partial_sum + a.totalHours, 0);

      // get total courses of semester rules (without course types)
      const semesterTotals = semesterRules.find(x => {
        return x.semester === studentSemester.semester && x.courseTypes && x.courseTypes.length === 0;
      });
      studentSemester.totalCourses = semesterTotals ? semesterTotals.courses : 0;
      studentSemester.totalUnits = semesterTotals ? semesterTotals.units : 0;
      studentSemester.totalEcts = semesterTotals ? semesterTotals.ects : 0;
      studentSemester.totalHours = semesterTotals ? semesterTotals.hours : 0;

      // check if there are rules for courseTypes
      const courseTypeRules = semesterRules.filter(x => {
        return x.semester === studentSemester.semester && x.courseTypes && x.courseTypes.length > 0;
      });
      studentSemester.rules = [];
      courseTypeRules.forEach(courseTypeRule => {
        const courseTypeStatisics = studentCourses.filter(x => {
          return studentSemester.semester === x.semester
            && courseTypeRule.courseTypes.map(y => y.id).includes(x.courseType)
        });
        let rule = {
          courseTypes: courseTypeRule.courseTypes,
          totals: {
            courses: courseTypeRule.courses || 0,
            units: courseTypeRule.units || 0,
            ects: courseTypeRule.ects || 0,
            hours: courseTypeRule.hours || 0,
            totalPassed: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalCourses, 0),
            totalPassedEcts: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalEcts, 0),
            totalPassedUnits: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalUnits, 0),
            totalPassedHours: courseTypeStatisics.reduce((partial_sum, a) => partial_sum + a.totalHours, 0),
          }
        };
        studentSemester.rules.push(rule);
      });
    });
    return studentSemesters;
  }

  @asyncMemoize()
  getGraduationRules(): any {
    return this._context.model('students/me/graduationRules')
      .asQueryable()
      .expand('validationResult')
      .take(-1)
      .getItems();
  }
  @asyncMemoize()
  getCourseTypes(): any {
    return this._context.model('courseTypes')
      .take(-1)
      .getItems();
  }
  @asyncMemoize()
  getLastExamPeriod() {
      return this._context.model('students/me/grades')
          .select('courseExam/year as gradeYear', 'courseExam/examPeriod as examPeriod')
          .orderByDescending('courseExam/year')
          .thenByDescending('courseExam/examPeriod')
          .take(-1)
          .getItem();
  }
  @asyncMemoize()
  getRecentGrades(): any {
    // get last examination period
    return this.getLastExamPeriod().then((lastExaminationPeriod) => {
      // get last grade year
      const lastGradeYear = lastExaminationPeriod && lastExaminationPeriod.gradeYear;
      // get last exam period
      const lastExamPeriod = lastExaminationPeriod && lastExaminationPeriod.examPeriod;
      if (typeof lastGradeYear === 'undefined' || typeof lastExamPeriod === 'undefined') {
        // return empty array
        return Promise.resolve([]);
      }
      // get courses (expand course attributes, exam instructors)
      return this._context.model('students/me/grades')
        .where('courseExam/year').equal(lastGradeYear)
        .and('courseExam/examPeriod').equal(lastExamPeriod)
        .expand('status', 'course($expand=gradeScale,locale)',
          'courseClass($expand=instructors($expand=instructor($select=InstructorSummary)))',
          'courseExam($expand=examPeriod,year)')
        .take(-1)
        .getItems();
    });
  }
  getPendingGrades(): any {
    // get last examination period
    return this.getLastExamPeriod().then((lastExaminationPeriod) => {
      // get last grade year
      const lastGradeYear = lastExaminationPeriod && lastExaminationPeriod.gradeYear;
      // get last exam period
      const lastExamPeriod = lastExaminationPeriod && lastExaminationPeriod.examPeriod;
      if (typeof lastGradeYear === 'undefined' || typeof lastExamPeriod === 'undefined') {
        // return empty array
        return Promise.resolve([]);
      }
      // get courses (expand course attributes, exam instructors)
      return this._context.model('students/me/grades')
        .where('courseExam/year').notEqual(lastGradeYear)
        .or('courseExam/examPeriod').notEqual(lastExamPeriod)
        .prepare()
        .and('status/alternateName').equal('pending')
        .expand('status', 'course($expand=gradeScale,locale)',
          'courseClass($expand=instructors($expand=instructor($select=InstructorSummary)))',
          'courseExam($expand=examPeriod,year)')
        .take(-1)
        .getItems();
    });
  }

  /**
   *
   * @param courses
   * @returnType: GradesAverageResult
   */
  getGradesSimpleAverage(courses: Array<CourseGradeBase>): GradeAverageResult {
    const average: GradeAverageResult = {
      courses: 0,
      passed: 0,
      grades: 0,
      coefficients: 0,
      average: 0,
      ects: 0,
      units: 0
    };
    if (!Array.isArray(courses)) {
      throw new Error('Courses must be an Array');
    }
    average.courses = courses.length;
    if (average.courses > 0) {
      // removes course parts
      const coursesArray = courses.filter(studentCourse => {
        const courseStructureType = studentCourse.courseStructureType &&
          ((typeof studentCourse.courseStructureType === 'number' && studentCourse.courseStructureType)
            || (typeof studentCourse.courseStructureType.id === 'number' && studentCourse.courseStructureType.id));
        return courseStructureType === 1 || courseStructureType === 4;
      });
      // we should add this statement in case the user only passed course parts in an exam period but hasn't
      // passed the other course parts so parent course grade will be null
      if (coursesArray.length > 0) {
        // gets the passed grades whose grade counts to the grade of the degree
        const courseUnits = coursesArray.filter(course => {
          return course.isPassed && course.calculateUnits;
        });
        if (courseUnits.length > 0) {
          average.units = courseUnits.map(studentCourse => {
            return studentCourse.units;
          }).reduce((sum, units) => sum + units);
          average.ects = courseUnits.map(course => {
            return course.ects;
          }).reduce((sum, ects) => sum + ects);

          average.passed = courseUnits.length;
        }
        const passedCourses = coursesArray.filter(course => {
          return course.calculateGrade && course.isPassed;
        });
        if (passedCourses.length > 0) {
          average.grades = Number (round((passedCourses.map(course => course.grade).reduce((sum, courseGrade) => sum + courseGrade)), 4));
          average.average = Number (round(average.grades / passedCourses.length, 4));
        }
      }
    }
    return average;
  }

  /**
   * This function takes a course array as a parameter and returns an
   * object which contains the total courses in the array, the passed grades, 
   * the sum of the ECTS of all the courses,
   * the sumOfGrades, the sum of the coefficients, the average and the sum
   * of ECTS and Units that the passed courses provide to the student.
   * @param courses
   * @returnType GradeAverageResult
   */
  getGradesWeightedAverage(courses: Array<CourseGradeBase>): GradeAverageResult {
    const average: GradeAverageResult = {
      courses: 0,
      passed: 0,
      grades: 0,
      coefficients: 0,
      average: 0,
      ects: 0,
      ectsTotal: 0,
      units: 0
    };
    if (!Array.isArray(courses)) {
      throw new Error('Courses must be an array');
    }
    if (courses.length === 0) {
      return average;
    } else {
      // removes course parts
      const filteredCourses = courses.filter(studentCourse => {
        const courseStructureType = studentCourse.courseStructureType &&
          ((typeof studentCourse.courseStructureType === 'number' && studentCourse.courseStructureType)
            || (typeof studentCourse.courseStructureType.id === 'number' && studentCourse.courseStructureType.id));
        return courseStructureType === 1 || courseStructureType === 4;
      });
      // we should add this statement in case the user only passed course parts in an exam period but hasn't
      // passed the other course parts so parent course grade will be null
      if (filteredCourses.length > 0) {
        //Calculate the sum of the ECTS of all the filtered courses
       average.ectsTotal = filteredCourses.map((course)=>course.ects).reduce((sum,ects)=>sum+ects,0);
        average.courses = filteredCourses.length;
        // We are filtering the courses that count to getting a degree
        const coursesUnits = filteredCourses.filter(studentCourse => {
          return studentCourse.calculateUnits && studentCourse.isPassed;
        });
        if (coursesUnits.length > 0) {
          average.units = coursesUnits.map(studentCourse => {
            return studentCourse.units;
          }).reduce((sum, units) => sum + units);
          average.passed = coursesUnits.length;
          average.ects = coursesUnits.map(course => {
            return course.ects;
          }).reduce((sum, ects) => sum + ects);
        }
        // We are filtering the passed courses that count to the grade of the degree
        const courseArray = filteredCourses.filter(course => {
          return course.calculateGrade &&
            course.coefficient &&
            course.coefficient > 0 &&
            course.isPassed;
        });
        if (courseArray.length > 0) {
          average.coefficients = courseArray.map(course => course.coefficient).reduce((sum, course) => sum + course);
          if (average.coefficients > 0) {
            average.grades = Number (round(courseArray.map(course => course.grade * course.coefficient)
              .reduce((sum, grade) => sum + grade) , 4));
            average.average = Number (round(average.grades / average.coefficients, 4));
          } else {
            return average;
          }
        }
      }
    }
    return average;
  }

  getGradesStatistics(courseExamId): any {
    return this._context.model(`/students/me/exams/${courseExamId}/statistics`)
      .asQueryable()
      // .groupBy('isPassed,examGrade,formattedGrade')
      // .select('count(id) as count,isPassed,examGrade,formattedGrade')
      .take(-1)
      .getItems();
  }

}
