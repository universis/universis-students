import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { LangComponent } from './students-shared/lang-component';

export const routes: Routes = [
    {
        path: 'lang/:id/:route',
        component: LangComponent
    },
    {
        path: 'lang/:id/:route/:subroute/:sub',
        component: LangComponent,
        pathMatch: 'prefix'
    },
    {
        path: 'lang/:id/:route/:subroute',
        component: LangComponent,
        pathMatch: 'prefix'
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'error/498',
        redirectTo: 'auth/loginAs'
    },
    {
        path: 'error/401.1',
        redirectTo: 'auth/loginAs'
    },
    {
        path: 'error/0',
        redirectTo: 'error/408.1'
    },
    {
        path: '',
        component: FullLayoutComponent,
        canActivate: [
            AuthGuard
        ],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                //loadChildren: './dashboard/dashboard.module#DashboardModule'
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'registrations',
                //loadChildren: './registrations/registrations.module#RegistrationsModule'
                loadChildren: () => import('./registrations/registrations.module').then(m => m.RegistrationsModule)
            },
            {
                path: 'grades',
                //loadChildren: './grades/grades.module#GradesModule'
                loadChildren: () => import('./grades/grades.module').then(m => m.GradesModule)
            },
            {
                path: 'profile',
                //loadChildren: './profile/profile.module#ProfileModule'
                loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
            },
            {
                path: 'requests',
                //loadChildren: './requests/requests.module#RequestsModule'
                loadChildren: () => import('./requests/requests.module').then(m => m.RequestsModule)
            },
            {
                path: 'messages',
                //loadChildren: './messages/messages.module#MessagesModule'
                loadChildren: () => import('./messages/messages.module').then(m => m.MessagesModule)
            },
            {
                path: 'info',
                //loadChildren: './info-pages/info-pages.module#InfoPagesModule'
                loadChildren: () => import('./info-pages/info-pages.module').then(m => m.InfoPagesModule)
            },
            {
                path: 'graduation',
                //loadChildren: './graduation-importer.module#GraduationImporterModule'
                loadChildren: () => import('./graduation-importer.module').then(m => m.GraduationImporterModule)
            },
            {
                path: 'events',
                loadChildren: () => import('./teaching-events/teaching-events.module').then(m => m.TeachingEventsModule)
            },
            {
                path: 'thesis-proposals',
                loadChildren: () => import('./thesis-proposals/thesis-proposals.module').then(m => m.ThesisProposalsModule)
            },
            {
                path: 'consents',
                //loadChildren: './info-pages/info-pages.module#InfoPagesModule'
                loadChildren: () => import('./consents/consents.module').then(m => m.ConsentsModule)
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        // enableTracing:true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
