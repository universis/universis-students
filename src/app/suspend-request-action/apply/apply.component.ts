import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ErrorService, LoadingService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ProfileService } from 'src/app/profile/services/profile.service';
import {ToastrService} from 'ngx-toastr';
import {AdvancedFormComponent, EmptyValuePostProcessor} from '@universis/forms';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html'
})
export class ApplyComponent implements OnInit, OnDestroy {

 // public data: any;
  public department: any;
  public student: any;
  public loading = true;
  public src = 'StudentSuspendActions/new';
  public continueLink = [ '/requests', 'list' ];
  public data = {};
  public isValid: boolean = false;
  public saved: boolean = false;
  public description: string | undefined = undefined;

  private queryParams: any;

  @Input() suspendRequestAction: any = {
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    }
  };
  @ViewChild('form', {
    static: false
  }) form: AdvancedFormComponent | undefined;
  @ViewChild('tabSet', {
    static: true
  }) tabSet: TabsetComponent | undefined;
  @ViewChild('agree') agree;

  queryParamSubscription: Subscription | undefined;

  constructor(
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _router: Router,
    private  _toastService: ToastrService,
    private _loadingService: LoadingService,
    private _profileService: ProfileService,
    private _activatedRoute: ActivatedRoute,
  ) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  async ngOnInit() {
    const student = await this._profileService.getStudent();
    this.showLoading(true);
    try {
      const documentConfiguration = await this._context.model('StudentRequestConfigurations')
      .where('additionalType').equal('RequestSuspendAction')
      .select('description')
      .getItem();
      this.description = documentConfiguration?.description;

      this.queryParamSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => this.queryParams = queryParams);
      if (!this.queryParams['code']) {
        this.suspendRequestAction = await this._context.model('RequestSuspendActions')
        .asQueryable()
        .and('suspensionYear').equal(student.department.currentYear.id || student.department.currentYear)
        .and('suspensionPeriod').equal(student.department.currentPeriod.id || student.department.currentPeriod)
        .orderByDescending('dateCreated')
        .expand('actionStatus')
        .getItem();

        if (this.suspendRequestAction == null) {
          Object.assign(
            this.data,
            {
              suspensionYear: student.department.currentYear,
              // tslint:disable-next-line:max-line-length
              suspensionPeriod: student.department.currentPeriod,
              student: student.id,
              name: this._translateService.instant('RequestSuspendActions.SuspendRequest')
            }
          );
        } else if (this.suspendRequestAction?.actionStatus?.alternateName === 'PotentialActionStatus') {
          this.data = this.suspendRequestAction;
        } else {
          return this._router.navigate(['/requests', 'RequestSuspendActions', this.suspendRequestAction.id, 'preview']);
        }
      } else {
        this.suspendRequestAction = await this._context.model('RequestSuspendActions')
        .asQueryable()
        .where('code').equal(this.queryParams['code'])
        .take(-1)
        .getItem();

        if (this.suspendRequestAction != null) {
          this.data = this.suspendRequestAction;
        }
      }
      this.showLoading(false);
    } catch(err) {
      console.error(err);
      this.showLoading(false);
    }
  }

  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
  }

  get activeTab(): any {
    if (this.tabSet) {
      return this.tabSet.tabs.find((tab) => {
        return tab.active;
      });
    }
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        return x;
      });
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }

  async beforeNext(nextTab: TabDirective): Promise<boolean> {
    try {
      if (nextTab.id === 'request-information') {
        return true;
      }

      // save application before attach documents
      if (nextTab.id === 'attach-documents') {
        // save action
        this._loadingService.showLoading();
        const formio = this.form?.form?.formio;
        const data = formio.data;
        formio.setPristine(false);
        // parse data with empty value processor
        new EmptyValuePostProcessor().parse(this.form?.formConfig, data);
        this.suspendRequestAction = await this._context.model('RequestSuspendActions').save(data);
        this.saved = true;
        this._loadingService.hideLoading();
      }
      return true;
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
      console.error(err);
      return false;
    } finally {
      this._loadingService.hideLoading();
    }
  }

  next(): void {
    // get active tab
    this.isValid = this.form?.form?.formio?.checkValidity(null, false, null, true);
    if (this.isValid) {
      const findIndex = this.tabSet?.tabs?.findIndex((tab) => {
        return tab.active;
      });
      if (findIndex != undefined && this.tabSet?.tabs != undefined && findIndex < this.tabSet?.tabs?.length) {
        // set active tab
        const tab = this.tabSet?.tabs[findIndex + 1];
        this.beforeNext(tab).then((result) => {
          if (result) {
            // disable current tab
            const currentTab = this.tabSet?.tabs[findIndex];
            if (currentTab) {
              currentTab.disabled = true;
            }
            // enable next tab
            tab.disabled = false;
            tab.active = true;
          }
        })
        .catch(err =>{ console.error(err)});
      }
    }
  }

  onDataChange(event: any): void {
    if (this.form && this.form.form) {
      // enable or disable button based on form status
      this.isValid = this.form?.form?.formio?.checkValidity(null, false, null, true);
    }
  }
}
