import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProgressBarSemesterComponent } from './progress-bar-semester.component';
import { GradeScale, GradeScaleService, ConfigurationService } from '@universis/common';
import {TestingConfigurationService} from '@universis/common/testing';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { GradesService } from 'src/app/grades/services/grades.service';

describe('ProgressBarSemesterComponent', () => {
  let component: ProgressBarSemesterComponent;
  let fixture: ComponentFixture<ProgressBarSemesterComponent>;

  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                        'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades']);
  const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

  profileSvc.getStudent.and.returnValue(Promise.resolve(JSON.parse('{"studyProgram":{"semesters":2},"studentStatus":{"alternateName":"active"}}')));
  gradeSvc.getAllGrades.and.returnValue(Promise.resolve(JSON.parse('[]')));
  const gradeScale = Object.assign(new GradeScale('en'), JSON.parse('{"formatPrecision":2, "scaleType":0, "scaleFactor":1}'));
  gradeSvc.getDefaultGradeScale.and.returnValue(Promise.resolve(gradeScale));

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ ProgressBarSemesterComponent ],
      providers: [
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
            provide: GradesService,
            useValue: gradeSvc
        },
        {
            provide: GradeScaleService,
            useValue: gradeScaleSvc
        },
        {
            provide: ConfigurationService,
            useClass: TestingConfigurationService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarSemesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
