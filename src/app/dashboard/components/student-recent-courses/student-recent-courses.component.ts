import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { template, at } from 'lodash';
import { ProfileService } from '../../../profile/services/profile.service';
import { CurrentRegistrationService } from '../../../registrations/services/currentRegistrationService.service';
import { ConfigurationService } from '@universis/common';

@Component({
  selector: 'app-student-recent-courses',
  templateUrl: './student-recent-courses.component.html',
  styleUrls: ['./student-recent-courses.component.scss']
})
export class StudentRecentCoursesComponent implements OnInit {

  public recentCourses: any = [];
  public currentRegistration: any;
  public isLoading = true;   // Only if data is loaded
  public currentRegistrarionEffectiveStatus: any;
  public defaultLanguage: string | any = "";
  public currentLanguage: string = "";
  public timetableExists = false;
  public hideElearningUrl: boolean = false;

  public registrationEdited = false;

  constructor(private _context: AngularDataContext, private _profileService: ProfileService,
    private _configurationService: ConfigurationService,
    private _currentReg: CurrentRegistrationService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService?.settings?.i18n?.defaultLocale;
  }

  ngOnInit() {
    if(this._configurationService.settings.app)
      this.hideElearningUrl = this._configurationService.settings.app['HideElearningUrl'] === true;

    this._profileService.getStudent().then(student => {
      this.registrationEdited = sessionStorage['registrationEdited'];
      this._context.model('TimetableEvents')
        .where('organizer').equal(student.department.id)
        .and('academicYear').equal(student.department.currentYear.id)
        .and('academicPeriods/id').equal(student.department.currentPeriod.id)
        .and('eventStatus/alternateName').equal('EventOpened')
        .getItem()
        .then(timetable => this.timetableExists = !!timetable)

      //  get current registration's effective status
      this._currentReg.getCurrentRegistrationEffectiveStatus().then(effectiveStatus => {
        if (effectiveStatus) {
          this.currentRegistrarionEffectiveStatus = effectiveStatus.status;
        }
        this._currentReg.getCurrentRegistration().then((currentReg: any = {}) => {
          if (currentReg == null) {
            this.currentRegistration = {
              student: student.id,
              registrationYear: student.department.currentYear,
              registrationPeriod: student.department.currentPeriod,
              classes: []
            };
          }
          if (currentReg && currentReg.classes) {
            this.generateClassAndELearningUrls(currentReg.classes).then((res) => {
              this.recentCourses = res;
              this.recentCourses.sort((a, b) => a.courseClass.course.name.localeCompare(b.courseClass.course.name));
              this.currentRegistration = currentReg;
            });
          }
          this.isLoading = false; // Data is loaded
        }).catch(err => {
          if (err.error.statusCode === 404) {
            this.currentRegistration = {
              student: student.id,
              registrationYear: student.department.currentYear,
              registrationPeriod: student.department.currentPeriod,
              classes: []
            };
            this.isLoading = false;
          }
        });
      });
    });
  }
  goToLink(url: string) {
    window.open(url, '_blank');
  }

  async generateClassAndELearningUrls(courses: Array<any>) {
    const student = await this._profileService.getStudent();
    if (student && student.department && student.department.organization && student.department.organization.instituteConfiguration) {
      const instituteConfig = student.department.organization.instituteConfiguration;
      courses.map(course => {
        if (course.courseClass.statistic?.studyGuideUrl) {
          course.courseClass.classUrl = course.courseClass.statistic.studyGuideUrl;
        } else if (instituteConfig.courseClassUrlTemplate) {
          course.courseClass.classUrl = template(instituteConfig.courseClassUrlTemplate)(course.courseClass);
        } else {
          course.courseClass.classUrl = '';
        }

        if (course.courseClass.statistic?.eLearningUrl) {
          course.courseClass.eLearningUrl = course.courseClass.statistic.eLearningUrl;
        } else if (instituteConfig.eLearningUrlTemplate) {
          course.courseClass.eLearningUrl = template(instituteConfig.eLearningUrlTemplate)(course.courseClass);
        } else {
          course.courseClass.eLearningUrl = '';
        }

        // remove eLearningUrl if HideElearningUrl is true
        if (this.hideElearningUrl) {
          course.courseClass.eLearningUrl = '';
        }
 
        // check if class section is present and get navigation links
        if (course.section) {
          this._context.model(`courseClasses/${course.courseClass.id}/sections`).getItems().then(sections => {
            if (sections && sections.length) {
              const section = sections.find(x => {
                return x.section === course.section;
              });
              if (section) {
                course.courseClass.navigationLinks = course.courseClass.navigationLinks || [];
                course.courseClass.navigationLinks.section = section.navigationLinks;
              }
            }
          });
        }
      });
    }
    return courses;
  }
}
