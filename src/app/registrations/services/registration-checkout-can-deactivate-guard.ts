import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { RegistrationCheckoutComponent } from '../components/registration-checkout/registration-checkout.component';
import { RegistrationLeaveComponent } from '../components/registration-leave-component/registration-leave.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalOptions } from 'ngx-bootstrap/modal/modal-options.class';

@Injectable()
export class RegistrationCheckoutCanDeactivateGuard implements CanDeactivate<RegistrationCheckoutComponent> {

  constructor(private modal: BsModalService) {
  }

  canDeactivate(
    component: RegistrationCheckoutComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> | boolean {

    if (!component.registrationEdited) {
      return true;
    }
    const subject = new Subject<boolean>();
    if (nextState && nextState.url.indexOf('registrations/courses/overview') > 0) {
      return true;
    }
    const config: ModalOptions = {
      animated: true,
      class: 'modal-dialog-centered modal-lg border-0',
      ignoreBackdropClick: true,
      keyboard: false
    };
    const modalRef: any = this.modal.show(RegistrationLeaveComponent, config);
    modalRef.content.subject = subject;
    return subject.asObservable();
  }
}
